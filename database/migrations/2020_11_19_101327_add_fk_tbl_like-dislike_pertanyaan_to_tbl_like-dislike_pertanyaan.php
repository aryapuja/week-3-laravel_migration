<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkTblLikeDislikePertanyaanToTblLikeDislikePertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_like-dislike_pertanyaan', function (Blueprint $table) {
            // FK tabel like-dislike pertanyaan - profile
                $table->unsignedBigInteger('id_profile');
                $table->foreign('id_profile')
                    ->references('id_profile')->on('tbl_profile')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
    
            // FK tabel like-dislike - pertanyaan
            Schema::table('tbl_like-dislike_pertanyaan', function (Blueprint $table) {
                $table->unsignedBigInteger('id_pertanyaan');
                $table->foreign('id_pertanyaan')
                    ->references('id_pertanyaan')->on('tbl_pertanyaan')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_like-dislike_pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['id_pertanyaan']);
            $table->dropColumn(['id_pertanyaan']);
            
            $table->dropForeign(['id_profile']);
            $table->dropColumn(['id_profile']);
        });
    }
}
