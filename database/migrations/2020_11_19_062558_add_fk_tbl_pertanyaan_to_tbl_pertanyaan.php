<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkTblPertanyaanToTblPertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // FK tabel pertanyaan - profile
        Schema::table('tbl_pertanyaan', function (Blueprint $table) {
            $table->unsignedBigInteger('id_profile');
            $table->foreign('id_profile')
                ->references('id_profile')->on('tbl_profile')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['id_profile']);
            $table->dropColumn(['id_profile']);
        });
    }
}
