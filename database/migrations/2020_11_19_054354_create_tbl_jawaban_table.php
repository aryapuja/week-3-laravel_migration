<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_jawaban', function (Blueprint $table) {
            $table->bigIncrements('id_jawaban');
            $table->string('isi_jawaban');
            $table->enum('status_jawaban',['biasa','favorit']);
            $table->integer('jml_vote');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_jawaban');
    }
}
