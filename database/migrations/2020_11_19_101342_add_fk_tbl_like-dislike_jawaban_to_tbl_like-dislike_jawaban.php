<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkTblLikeDislikeJawabanToTblLikeDislikeJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_like-dislike_jawaban', function (Blueprint $table) {
            // FK tabel like-dislike jawaban - profile
                $table->unsignedBigInteger('id_profile');
                $table->foreign('id_profile')
                    ->references('id_profile')->on('tbl_profile')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
    
            // FK tabel like-dislike jawaban - jawaban
            Schema::table('tbl_like-dislike_jawaban', function (Blueprint $table) {
                $table->unsignedBigInteger('id_jawaban');
                $table->foreign('id_jawaban')
                    ->references('id_jawaban')->on('tbl_jawaban')
                    ->onUpdate('cascade')->onDelete('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_like-dislike_jawaban', function (Blueprint $table) {
            $table->dropForeign(['id_jawaban']);
            $table->dropColumn(['id_jawaban']);
            
            $table->dropForeign(['id_profile']);
            $table->dropColumn(['id_profile']);
        });
    }
}
