<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PertanyaanModel extends Model
{
    protected $table = 'tbl_pertanyaan';
    // protected $fillable = ["isi_pertanyaan", "jml_vote", "id_profile"];
    // protected $guarded = []; //tabel-tabel yang tidak boleh diisi
}
