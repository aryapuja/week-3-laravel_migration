<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $posts = DB::table('tbl_pertanyaan')->get();
        // dd($post);
        return view('konten.v_index',compact('posts'));
    }

    public function create()
    {
        return view('konten.v_create');

    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'isi_pertanyaan' => 'required|max:255|unique:tbl_pertanyaan'
        ]);

        $query = DB::table('tbl_pertanyaan')->insert([
            "isi_pertanyaan"    => $request['isi_pertanyaan'],
            "jml_vote"          => 0,
            "created_at"        => date('Y-m-d H:i:s'),
            "updated_at"        => date('Y-m-d H:i:s'),
            "id_profile"        => 1
        ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil dibuat');
    }

    public function show($id_pertanyaan)
    {
        $post = DB::table('tbl_pertanyaan')->where('id_pertanyaan', $id_pertanyaan)->first();
        // dd($post);
        return view('konten.v_show',compact('post'));
    }

    public function edit($id_pertanyaan)
    {
        $post = DB::table('tbl_pertanyaan')->where('id_pertanyaan', $id_pertanyaan)->first();
        return view('konten.v_edit',compact('post'));
    }

    public function update($id_pertanyaan, Request $request)
    {
        $request->validate([
            'isi_pertanyaan' => 'required|max:255|unique:tbl_pertanyaan'
        ]);
        $query = DB::table('tbl_pertanyaan')
                ->where('id_pertanyaan', $id_pertanyaan)
                ->update([
                    "isi_pertanyaan"    => $request['isi_pertanyaan'],
                    "updated_at"        => date('Y-m-d H:i:s')
                ]);
        return redirect('/pertanyaan',)->with('success','Pertanyaan berhasil diupdate');
        
    }

    public function delete($id_pertanyaan)
    {
        $query = DB::table('tbl_pertanyaan')
                ->where('id_pertanyaan', $id_pertanyaan)->delete();
        return redirect('/pertanyaan',)->with('success','Pertanyaan berhasil dihapus');
        
    }

    

}
