<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PertanyaanModel;

class NewPertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = PertanyaanModel::all();
        return view('konten.v_index',compact('posts'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('konten.v_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //metode save
        $post = new PertanyaanModel;
        $post->isi_pertanyaan = $request['isi_pertanyaan'];
        $post->jml_vote = 0;
        $post->id_profile = 1;
        $post->save();
        
        //metode fillable & guarded
        // $post = PertanyaanModel::create([
        //     'isi_pertanyaan'=>$request['isi_pertanyaan'],
        //     'jml_vote'=>0,
        //     'id_profile'=>1,
        // ]);

        return redirect('/pertanyaan')->with('success','Pertanyaan berhasil dibuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = PertanyaanModel::where('id_pertanyaan',$id)->first();
        return view('konten.v_show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = PertanyaanModel::where('id_pertanyaan',$id)->first();
        return view('konten.v_edit',compact('post'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = PertanyaanModel::where('id_pertanyaan', $id)
                ->update([
                    "isi_pertanyaan" => $request['isi_pertanyaan']
                ]);
        return redirect('/pertanyaan',)->with('success','Pertanyaan berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = PertanyaanModel::where('id_pertanyaan',$id)->delete();
        return redirect('/pertanyaan',)->with('success','Pertanyaan berhasil dihapus');
    }
}
