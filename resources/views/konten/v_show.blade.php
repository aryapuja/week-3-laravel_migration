@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3 mr-3 ">
        <p>Id Penanya: {{$post->id_profile}}</p>
        <p>Pertanyaan: {{$post->isi_pertanyaan}}</p>
        <p>Tanggal Pertanyaan Dibuat: {{$post->created_at}}</p>
        <p>Tanggal Pertanyaan Diupdate: {{$post->updated_at}}</p>
        <p>Jumlah Vote untuk Pertanyaan Ini: {{$post->jml_vote}}</p>
    </div>
    
@endsection