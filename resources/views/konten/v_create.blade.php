@extends('adminlte.master')

@section('content')
    <div class="ml-3 mt-3 mr-3 ">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Ajukan Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/pertanyaan" method="POST">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="isi_pertanyaan">Isi Pertanyaan</label>
              <input type="text" class="form-control" id="isi_pertanyaan" name="isi_pertanyaan" placeholder="Masukkan Pertanyaan" value = "{{old('isi_pertanyaan', '')}}">
              @error('isi_pertanyaan')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
            </div>
          </div>
          <!-- /.card-body -->
      
          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat Pertanyaan</button>
          </div>
        </form>
      </div>
    </div>
    
@endsection