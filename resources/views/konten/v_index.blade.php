@extends('adminlte.master')

@section('content')
    <div class="row">
      <div class="col-xl-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Daftar Pertanyaan yang Sudah Dibuat </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success" role="alert">
                  {{session('success')}}
                </div>
            @endif
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Isi Pertanyaan</th>
                  <th>ID Penanya</th>
                  <th>Dibuat Pada</th>
                  <th>Diupdate Pada</th>
                  <th>Jumlah Vote Saat Ini</th>
                  <th>action</th>
                </tr>
              </thead>
              <tbody>

              @forelse ($posts as $key => $isi)
                <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$isi->isi_pertanyaan}}</td>
                  <td>{{$isi->id_profile}}</td>
                  <td>{{$isi->created_at}}</td>
                  <td>{{$isi->updated_at}}</td>
                  <td>{{$isi->jml_vote}}</td>
                  <td style="display: flex">
                    <a href="{{route('pertanyaan.show', ['pertanyaan'=>$isi->id_pertanyaan])}}" class="btn btn-primary btn-sm">Show</a>
                    <a href="/pertanyaan/{{$isi->id_pertanyaan}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <form action="/pertanyaan/{{$isi->id_pertanyaan}}" method="POST">
                      @csrf
                      @method('DELETE')        
                      <input type="submit" name="delete" id="delete" value='Hapus' class="btn btn-danger btn-sm">
                    </form>
                  </td>
                </tr>
              @empty
                  <td colspan="7" align="center">Tidak ada List Pertanyaan</td>
              @endforelse
                
                {{-- @foreach ($posts as $key => $isi)
                  <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$isi->isi_pertanyaan}}</td>
                    <td>{{$isi->id_profile}}</td>
                    <td>{{$isi->created_at}}</td>
                    <td>{{$isi->updated_at}}</td>
                    <td>{{$isi->jml_vote}}</td>
                    <td>Field untuk Button</td>
                  </tr>
                @endforeach --}}
              </tbody>
              <tfoot>
                <tr>
                  <th>Nomor</th>
                  <th>Isi Pertanyaan</th>
                  <th>ID Penanya</th>
                  <th>Dibuat Pada</th>
                  <th>Diupdate Pada</th>
                  <th>Jumlah Vote Saat Ini</th>
                  <th>action</th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>    
    </div>
@endsection

@push('scripts_datatable')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush